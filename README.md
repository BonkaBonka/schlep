# Schlep

Watch a beanstalkd queue and execute a command on each job.

## Building

~~~
$ vgo build
~~~

## Stuff that needs ("needs"?) doing:

### Immediate

  * Replace [factorlog](https://github.com/kdar/factorlog) with [seelog](https://github.com/cihub/seelog).
  * Replace [go-flags](https://github.com/jessevdk/go-flags) with [pflag](https://github.com/spf13/pflag).

### Longer-term

  * Use a config file to spin up multiple workers.  The `configfile` branch is a failed start at this.

### Eventually

  * Tests (I guess).

### Administrivia

  * Find a better name than "schlep" (beanstalkd-worker perhaps).
