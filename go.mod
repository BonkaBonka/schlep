module kirk.lan/git/schlep

require (
	github.com/iwanbk/gobeanstalk v0.0.0-20160903043409-dbbb23937c31
	github.com/jessevdk/go-flags v1.4.0
	github.com/kdar/factorlog v0.0.0-20140929220826-d5b6afb8b4fe
	github.com/mattn/go-colorable v0.0.9 // indirect
	github.com/mattn/go-isatty v0.0.4 // indirect
	github.com/mgutz/ansi v0.0.0-20170206155736-9520e82c474b // indirect
)
