package main

import (
	"fmt"
	"os"
	"os/exec"
	"os/signal"
	"regexp"
	"strconv"
	"syscall"
	"time"

	"github.com/iwanbk/gobeanstalk"
	"github.com/jessevdk/go-flags"
	"github.com/kdar/factorlog"
)

func main() {
	logformat := "[%{Date} %{Time}]\t%{SEVERITY}\t%{SafeMessage}"
	log := factorlog.New(os.Stdout, factorlog.NewStdFormatter(logformat))
	log.SetMinMaxSeverity(factorlog.INFO, factorlog.PANIC)

	var opts struct {
		Command        string `short:"c" long:"command" description:"the command to run" required:"true"`
		Server         string `short:"s" long:"server" description:"beanstalk server address" default:"localhost:11300"`
		Tube           string `short:"t" long:"tube" description:"beanstalk tube to watch" default:"default"`
		OutName        string `short:"o" long:"output" description:"output filename format" default:"/tmp/job-%d.out"`
		TouchFrequency int    `long:"touch" description:"how often to touch running jobs" default:"0"`
		Verbose        func() `short:"v" long:"verbose" description:"Enable all logging output"`
		Nobury         bool   `long:"nobury" description:"Delete failed jobs instead of burying them" default:"false"`
	}

	opts.Verbose = func() {
		log.SetMinMaxSeverity(factorlog.NONE, factorlog.PANIC)
	}

	leftovers, err := flags.Parse(&opts)
	if err != nil || len(leftovers) > 0 {
		log.Fatal("CLI options parsing failed")
	}

	Worker(opts.Server, opts.Tube, opts.Command, opts.OutName, opts.TouchFrequency, opts.Nobury, log)
}

func Worker(server string, tube string, command string, outname string, touchfrequency int, nobury bool, log *factorlog.FactorLog) {
	priorityRegex := regexp.MustCompile("\\spri:\\s(\\d+)\\s")

	log.Debugf("Server      : '%s'", server)
	log.Debugf("Tube        : '%s'", tube)
	log.Debugf("Command     : '%s'", command)
	log.Debugf("Output Name : '%s'", outname)
	log.Debugf("Touch Freq. : '%d'", touchfrequency)
	log.Debugf("No Bury     : '%t'", nobury)

	log.Tracef("Connecting to '%s'", server)

	beanstalk, err := gobeanstalk.Dial(server)
	if err != nil {
		log.Fatal(err)
	}
	defer beanstalk.Quit()

	if tube != "default" {
		log.Tracef("Switching watch tube to '%s'", tube)
		tubeCount, err := beanstalk.Watch(tube)
		if err != nil {
			log.Fatal(err)
		}
		tubeCount, err = beanstalk.Ignore("default")
		if err != nil {
			log.Fatal(err)
		}
		if tubeCount != 1 {
			log.Fatal("More than one tube being watched!")
		}
	}

	exiting := false
	for !exiting {
		log.Trace("Waiting to reserve a job ...")
		job, err := beanstalk.Reserve()
		if err != nil {
			log.Fatal(err)
		}

		log.Infof("Reserve (%d) : '%s'", job.ID, string(job.Body))

		log.Tracef("        (%d) : Requesting job info", job.ID)
		var burypriority uint32
		yaml, err := beanstalk.StatsJob(job.ID)
		if err != nil {
			log.Fatal(err)
		}
		log.Debugf("JobInfo (%d) : '%s'", job.ID, yaml)

		match := priorityRegex.FindSubmatch(yaml)
		if match != nil {
			log.Debugf("        (%d) : priority regex match '%s'", job.ID, match[1])
			val, err := strconv.ParseUint(string(match[1]), 10, 32)
			if err != nil {
				log.Fatal(err)
			}
			burypriority = uint32(val)
		}

		var sigchan = make(chan os.Signal, 1)
		signal.Notify(sigchan, os.Interrupt, syscall.SIGTERM)

		var cmd *exec.Cmd

		var runner = make(chan bool)
		go func() {
			var filename = ""
			var out *os.File = nil

			if outname != "" {
				filename = fmt.Sprintf(outname, job.ID)

				log.Debugf("        (%d) : Output file '%s'", job.ID, filename)

				out, err = os.Create(filename)
				if err != nil {
					log.Fatal(err)
				}
				defer func() {
					err := out.Close()
					if err != nil {
						log.Fatal(err)
					}
				}()
			}

			log.Debugf("Execute (%d) : '%s' '%s'", job.ID, command, string(job.Body))

			cmd = exec.Command("/bin/sh", "-cex", command, "", string(job.Body))
			cmd.Stdout = out
			cmd.Stderr = out

			success := cmd.Run() == nil
			if success && filename != "" {
				log.Tracef("Remove  (%d) : '%s'", job.ID, filename)
				err = os.Remove(filename)
				if err != nil {
					log.Fatal(err)
				}
			}

			runner <- success
		}()

	ExecLoop:
		for {
			var touch <-chan time.Time
			if touchfrequency > 0 {
				touch = time.After(time.Duration(touchfrequency) * time.Second)
			}

			select {
			case success := <-runner:
				if success || nobury {
					log.Infof("Delete  (%d) : Command exited - success %t", job.ID, success)
					err = beanstalk.Delete(job.ID)
					if err != nil {
						log.Fatal(err)
					}
				} else {
					log.Warnf("Bury    (%d) : Command exited - success %t", job.ID, success)
					err = beanstalk.Bury(job.ID, burypriority)
					if err != nil {
						log.Fatal(err)
					}
				}
				break ExecLoop
			case <-touch:
				err = beanstalk.Touch(job.ID)
				if err != nil {
					log.Errorf("Touch   (%d) : %s", job.ID, err)
				}
			case <-sigchan:
				log.Warn("Signal received - terminating")
				if err := cmd.Process.Kill(); err != nil {
					log.Fatal("failed to kill: ", err)
				}
				exiting = true
			}
		}
	}

	log.Warn("Shutting down")
}
